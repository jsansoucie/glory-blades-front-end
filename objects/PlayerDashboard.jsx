import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import CombatantDisplay from './CombatantDisplay.jsx';

class PlayerDashboard extends React.Component {
  componentDidMount() {
    /* Here we would make a database call to previous combatants to fill the fields. For now I'll
    fill it with random junk */

  }

  quickBattle()
  {
    alert("Load up the quick battle screen here for the player to choose an enemy or choose random.");
  }

  storyMode()
  {
    alert("Load up the story board here and load any saved progress on the player.");
  }

  settings()
  {
    alert("Load up the settings screen for difficulty of the game, player's name, etc...");
  }

  render() {
    var comb1 = <CombatantDisplay cid={'_0'} combatantClass='thief' combatantName='Bob' gender='male' />;
    var comb2 = <CombatantDisplay cid={'_1'} combatantClass='fighter' combatantName='Jim' gender='male' />;
    var comb3 = <CombatantDisplay cid={'_2'} combatantClass='barbarian' combatantName='Jim' gender='male' />;
    var comb4 = <CombatantDisplay cid={'_3'} combatantClass='mage' combatantName='Jim' gender='male' />;

    return(
      <div id='pdb_main_div'>
        <div id='pdb_combatant_list_div'>
          { comb1 }
          { comb2 }
          { comb3 }
          { comb4 }
        </div>
        <div id='pdb_welcome_text_div'>
          Welcome {this.props.name}!!
        </div>
        <div id='pdb_actions_container_div'>
          <div id='pdb_quick_battle_div' onClick={this.quickBattle}>
            <div className='action_text'>Quick Battle</div>
            <img className='door_knob' src={require('../resources/doorknob.png')} alt='O' />
          </div>
          <div id='pdb_story_mode_div' onClick={this.storyMode}>
            <div className='action_text'>Story Mode</div>
            <img className='door_knob' src={require('../resources/doorknob.png')} alt='O' />
          </div>
          <div id='pdb_settings_div' onClick={this.settings}>
            <div className='action_text'>Settings</div>
            <img className='door_knob' src={require('../resources/doorknob.png')} alt='O' />
          </div>
        </div>
      </div>
    );
  }
}

export default PlayerDashboard;
