import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import LogInScreen from './LogInScreen.jsx';

class SplashScreen extends React.Component {
  loadGame() {
    ReactDOM.render(<LogInScreen />, document.getElementById('app'));
  }

  render() {
    return(
      <div id='ss_main_div'>
        <div id='ss_image_div'>
          <img id='splash_screen_image_id' src={require('../resources/splash_screen_image.png')}
          alt='Logo' onClick={this.loadGame}/>
        </div>
        <div id='ss_text'>Click the title image to begin...</div>
      </div>
    );
  }
};

export default SplashScreen;
