import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import PlayerDashboard from './PlayerDashboard.jsx';

class LogInScreen extends React.Component {
  logIn() {
    ReactDOM.render(<PlayerDashboard name='Ghost' />, document.getElementById('app'));
  }

  render() {
    return(
      <div id='login_master_div'>
        <div id='login_pic_div'>
          <img id='login_screen_pic' src={require('../resources/swords.png')} alt='Swords' />
        </div>
        <div id='login_form_div'>
          <input type='text' id='username' placeholder='Username'/>
          <input type='text' id='password' placeholder='Password'/>
          <button id='login_button' onClick={this.logIn}>Log In</button>
        </div>
      </div>
    );
  }
}

export default LogInScreen;
