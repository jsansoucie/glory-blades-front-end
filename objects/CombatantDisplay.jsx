import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';

class CombatantDisplay extends React.Component {
  render() {
    return(
      <div id={'c_id_' + this.props.cid} className='combatant_display_div'>
        <img src={require('../resources/' + this.props.combatantClass + '_img.jpg')} className='combatant_display_image' />
        <div className='combatant_display_info_div'>
          <div className='combatant_display_info_name'>{this.props.combatantName}</div>
          <div className='combatant_display_info_class'>{this.props.combatantClass}</div>
          <div className='combatant_display_info_gender'>{this.props.gender}</div>
        </div>
      </div>
    );
  }
}

export default CombatantDisplay;
