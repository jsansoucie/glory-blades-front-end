const webpack = require('webpack');
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

const isProd = process.env.NODE_ENV === "production";

const config = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader?cacheDirectory',
        exclude: [/node_modules/]
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader'],
        exclude: [/node_modules/]
      },
      {
        test: /\.css$/,
        loader: 'css-loader',
      },
      {
        test: /\.pug$/,
        loader: 'pug-loader',
        exclude: [/node_modules/]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000',
      },
      {test: /\.(jpe?g|png|gif)$/i, loader: 'file-loader?name=[name].[ext]'},
      {test: /\.ico$/, loader: 'file-loader?name=[name].[ext]'},
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  // Change Entry Path To Match Source JS
  entry: path.resolve(__dirname, "index.js"),
  // Change Output Path To Desired Directory
  output: {
    path: path.resolve(__dirname, "app"),
    filename: "bundle.[hash].js"
  },
  devtool: isProd ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',
  devServer: {
      port: 3887,
      contentBase:  path.resolve(__dirname, "app")
  },
  plugins: [
    // Choose Input (Template) & Output (Filename)
    new htmlWebpackPlugin({ filename: 'index.html', template: 'index.pug', xhtml: true }),
    new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development') })
  ]
}

module.exports = config;
