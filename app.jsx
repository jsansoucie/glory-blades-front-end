import React from 'react';
import SplashScreen from './objects/SplashScreen.jsx';
require('./main_style.scss');

class App extends React.Component
{
  render()
  {
    return(
      <SplashScreen />
    );
  }
};

export default App;
